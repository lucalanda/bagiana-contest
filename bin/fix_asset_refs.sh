#!/bin/bash

prefix=/bagiana-contest

current_path=`dirname $(realpath $0)`
index_html_path=${current_path}/../dist/index.html

sed -i "s|href=\"/|href=\"${prefix}/|g" $index_html_path
sed -i "s|src=\"/|src=\"${prefix}/|g" $index_html_path
