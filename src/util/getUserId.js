const USER_ID = 'user_id';
import {USER_ID_LENGTH} from '../config/config';

const localStorageSupported = typeof (Storage) !== "undefined";

const getUserId = () => {
  if (localStorageSupported) {
    return getOrSetUserId();
  } else {
    return generateRandomUserId();
  }
};

const getOrSetUserId = () => {
  let userId = localStorage.getItem(USER_ID);

  if (userId == null) {
    userId = generateRandomUserId();
    localStorage.setItem(USER_ID, userId);
  }

  return userId;
}

const generateRandomUserId = () => {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < USER_ID_LENGTH; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
};

export default getUserId;
