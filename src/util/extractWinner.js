const extractWinner = (bagianaIds) => {
  const occurrences = {};
  bagianaIds.forEach((id) => {
    if (!occurrences[id]) {
      occurrences[id] = 0;
    }

    occurrences[id]++;
  });

  const sortedIds = bagianaIds.sort((a, b) => {
    const result = occurrences[a] - occurrences[b];

    if (result !== 0) return result;
    else return a < b ? -1 : 1;
  });

  return sortedIds.pop();
};

export default extractWinner;
