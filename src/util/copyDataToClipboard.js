const getDataStringified = (bagianas) => bagianas
  .map(({ author, title }) => `* ${author}: ${title}`)
  .join("\n");

const copyStringToClipboard = (string) => {
  const dummyTextArea = document.createElement('textarea');
  dummyTextArea.value = string;
  document.body.appendChild(dummyTextArea);
  dummyTextArea.select();
  document.execCommand('copy');
  document.body.removeChild(dummyTextArea);
};

const copyDataToClipboard = (bagianas) => {
  const result = getDataStringified(bagianas);
  copyStringToClipboard(result);
};

export default copyDataToClipboard;
