import { ROOM_NAME_PARAM } from '../config/config';

const getRoomName = () => {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.get(ROOM_NAME_PARAM);
};

export default getRoomName;
