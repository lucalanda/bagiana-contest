const joinOrCreateRoom = (name) => {
  const currentLocation = window.location.href.split('?')[0];
  window.location = currentLocation + '?room=' + name;
};

export default joinOrCreateRoom;
