import setIntervals from "./setIntervals";
import sleep from "./sleep";

const animationIntervals = [
  { size: 50, duration: 1000 },
  { size: 60, duration: 500 },
  { size: 70, duration: 500 },
  { size: 80, duration: 500 },
  { size: 100, duration: 500 },
  { size: 150, duration: 500 },
  { size: 200, duration: 750 },
  { size: 300, duration: 2100 },
];

class BagianaSelectionAnimator {
  constructor(opponents, winnerIndex) {
    this.opponents = opponents;
    this.winnerIndex = winnerIndex;
    this.currentIndex = 0;
  }

  onNameEmitted(callback) {
    this.onNameEmittedCallback = callback;
    return this;
  }

  onEnd(callback) {
    this.onEndCallback = callback;
    return this;
  }

  async start() {
    setIntervals(() => this.emitNextName(), animationIntervals, () => this.endAnimation());
  }

  async endAnimation() {
    await this.emitNamesUntilWinner();

    this.onEndCallback();
  }

  async emitNamesUntilWinner() {
    const interval = animationIntervals[animationIntervals.length - 1].size;

    while (this.currentIndex !== this.winnerIndex) {
      await sleep(interval);
      this.emitNextName();
    }
  }

  emitNextName() {
    this.increaseCurrentIndex();
    this.onNameEmittedCallback(this.opponents[this.currentIndex]);
  }

  increaseCurrentIndex() {
    this.currentIndex = (this.currentIndex + 1) % this.opponents.length;
  }
}

export default BagianaSelectionAnimator;
