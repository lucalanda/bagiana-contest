import sleep from './sleep';

const setIntervals = async (callback, intervals, onFinished) => {
  for (let intervalObj of intervals) {
    const { size, duration } = intervalObj;
    const interval = setInterval(callback, size);
    await sleep(duration);
    clearInterval(interval);
  }

  onFinished();
};

export default setIntervals;
