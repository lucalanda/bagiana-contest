const getDebugModeFlag = () => {
  const envVar = process.env.VUE_APP_DEBUG;

  if (envVar == "false") return false;
  return true;
}

export const DEBUG = getDebugModeFlag();
export const MIN_VOTES = 1;
export const ROOM_NAME_REGEX = /^(?:(?![×Þß÷þø])[-'0-9A-Za-zÀ-ÿ\s])+$/;
export const ROOM_NAME_PARAM = 'room';
export const USER_ID_LENGTH = 20;
