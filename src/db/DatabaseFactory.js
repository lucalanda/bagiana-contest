import Database from './Database';
import DatabaseMock from './DatabaseMock';

class DatabaseFactory {
  constructor(debugMode) {
    this.debugMode = debugMode;
  }

  create(roomName, userId) {
    return this.debugMode
      ? new DatabaseMock(roomName, userId)
      : new Database(roomName, userId);
  }
}

export default DatabaseFactory;
