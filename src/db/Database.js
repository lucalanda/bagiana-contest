import firebase from "firebase/app";
import "firebase/database";
import firebaseConfig from './firebaseConfig';
import { PENDING_VOTES } from '../config/appStates';

export default class Database {
  constructor(roomName, userId) {
    this.roomName = roomName;
    this.userId = userId;
    this.db = null;
  }

  connect() {
    firebase.initializeApp(firebaseConfig);
    this.db = firebase.database();
  }

  onBagianasUpdate(callback) {
    this.db
      .ref(`room/${this.roomName}/bagianas`)
      .on('value', (snapshot) => {
        const records = [];

        if (snapshot.exists()) {
          const obj = snapshot.val();
          for (let id in obj) {
            records.push({ id, ...obj[id] })
          }
        }

        callback(records)
      });
  }

  onVotesUpdate(callback) {
    this.db
      .ref(`room/${this.roomName}/votes`)
      .on('value', (snapshot) => {
        const records = [];

        if (snapshot.exists()) {
          const votesMap = snapshot.val();
          for (let userId in votesMap) {
            records.push({ voterId: userId, bagianaId: votesMap[userId] })
          }
        }

        callback(records)
      });
  }

  onStateUpdate(callback) {
    this.db
      .ref(`room/${this.roomName}/state`)
      .on('value', (snapshot) => {
        callback(snapshot.val());
      })
  }

  addBagiana(author, title) {
    const newBagianaRef = this.db
      .ref(`room/${this.roomName}/bagianas`)
      .push();

    newBagianaRef.set({ title, author });
  }

  deleteBagiana(id) {
    this.db
      .ref(`room/${this.roomName}/bagianas/${id}`)
      .remove();

    this.deleteVotesForBagiana(id);
  }

  deleteVotesForBagiana(id) {
    const votesRef = this.db.ref(`room/${this.roomName}/votes`);

    votesRef.once('value', (snapshot) => {
      if (snapshot.exists()) {
        const votes = snapshot.val();
        Object.entries(votes).forEach(([voterId, bagianaId]) => {
          if (bagianaId == id) votesRef.child(voterId).remove();
        })
      }
    });
  }

  vote(id) {
    this.db
      .ref(`room/${this.roomName}/votes/${this.userId}`)
      .set(id);
  }

  updateState(newState) {
    this.db
      .ref(`room/${this.roomName}/state`)
      .set(newState);
  }

  clearRoom() {
    this.db
      .ref(`room/${this.roomName}/`)
      .set({ state: PENDING_VOTES });
  }
}
