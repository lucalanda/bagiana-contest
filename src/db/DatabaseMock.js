import { PENDING_VOTES } from '../config/appStates';
import schemaExample from '../../db/schema_example.json';

const INITIAL_STATE = {
  state: PENDING_VOTES,
  bagianas: [],
  votes: []
}

class DatabaseMock {
  

  constructor(_, userId) {
    this.userId = userId;
    Object.assign(this, INITIAL_STATE);
  }

  connect() {
    this.setDefaultBagianas();
  }

  setDefaultBagianas() {
    const exampleBagianas = schemaExample.room.firstRoom.bagianas;

    this.bagianas = Object.entries(exampleBagianas).map(([id, data]) => ({ id, ...data }))
  }

  onBagianasUpdate(callback) {
    this.onBagianasUpdateCallback = callback;
    this.emitBagianas();
  }

  onVotesUpdate(callback) {
    this.onVotesUpdate = callback;
    this.emitVotes();
  }

  onStateUpdate(callback) {
    this.onStateUpdateCallback = callback;
    this.emitState();
  }

  addBagiana(author, title) {
    this.bagianas.push({ author, title, id: Math.random().toString() });
    this.emitBagianas();
  }

  deleteBagiana(id) {
    const index = this.bagianas.findIndex(({ id: bagianaId }) => id === bagianaId);
    this.bagianas.splice(index, 1);
    this.emitBagianas();
  }

  vote(id) {
    this.votes = [{ voterId: this.userId, bagianaId: id }];
    this.emitVotes();
  }

  updateState(newState) {
    this.state = newState;
    this.emitState();
  }

  clearRoom() {
    Object.assign(this, INITIAL_STATE);
    this.emitBagianas();
    this.emitVotes();
    this.emitState();
  }

  emitBagianas() {
    this.onBagianasUpdateCallback(this.bagianas);
  }

  emitVotes() {
    this.onVotesUpdate(this.votes);
  }

  emitState() {
    this.onStateUpdateCallback(this.state);
  }
}

export default DatabaseMock;
