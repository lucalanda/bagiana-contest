module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/styles/abstracts/_variables.scss";
          @import "@/styles/abstracts/_mixins.scss";
        `
      }
    }
  }
};
